package com.example.record;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class RecordTest {
    @Test
    void itShouldBeImmutableRecord() {
        record Foo(int id, String name){}
        Foo foo = new Foo(1, "foo");
    }

    @Test
    void itShouldBeMutableRecord() {
        record Bar(int id, String name, List<String> items) {}

        List<String> items = new ArrayList<>();
        items.add("1");
        Bar bar = new Bar(1, "bar", items);
        items.add("2");
        bar.items().add("3");

        Assertions.assertEquals(3, items.size());
    }

    @Test
    void itShouldBeMutableRecordAndReturnException() {
        // Making the record actually be immutable
        record Bar(int id, String name, List<String> items) {
            Bar {
                items = List.copyOf(items);
            }
        }

        List<String> items = new ArrayList<>();
        items.add("1");
        Bar bar = new Bar(1, "bar", items);
        items.add("2");

        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            bar.items().add("3");
        });
    }

}
