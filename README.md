##immutable-record

A new type called a “record” was previewed in Java 14 and released in Java 16. One of the benefits is that it creates immutable objects